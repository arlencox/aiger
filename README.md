AIGER outputs sequential and combinational And-Inverter graphs in the AIGER 1
text-mode format.  It uses the AAG type header and can be converted into
the binary AIGER format using the `aigtoaig` tool provided in the
[AIGER package](http://fmv.jku.at/aiger/).

It hash-conses the generated formulas to reduce the representation and to
ensure good use of the AIG file format.  Additionally, it performs
on-the-fly simplification and constant propagation.  It does not support
advanced simplification techniques (such as sweeping), but all constants are
correctly propagated.

The generated file is compatible with model checkers that support the AAG
input type or support the binary AIGER format that can be generated using
the `aigtoaig` tool.

An And-Inverter graph (AIG) is made up of the following elements:

- Inputs are named, single-bit inputs from external sources.
- Outputs are named, single-bit outputs to external sources.  They are
  used for properties in model checking.
- And gates are two-input operations that perform the logical **AND**
  of the two inputs.  That is, the output is true only if both inputs
  are true.
- Inverter gates are single input operations that negate the input.
- Registers are named sequential elements that pass their input through
  to their output whenever a clock edge occurs.  Clocks are implicit in
  AIGER representation. 

All other operations are constructed using these basic operations.

Since AIGER does not support named registers and having register names is
useful for debugging (especially when simplification has been applied),
register names are included as comments at the end of the file.

Experimental support is available for AIGER 1 binary mode format.  This
provides a more compact output and does not require conversion using the
`aigtoaig` tool to be used with an off-the-shelf hardware model checking
tool like [abc](http://www.eecs.berkeley.edu/~alanmi/abc/) or
[IIMC](http://ecee.colorado.edu/wpmu/iimc/).
