VERSION = 0.1

.PHONY: default all opt install doc
default: all opt META
all: aiger.cma
opt: aiger.cmxa

META: META.in Makefile
	sed -e 's:@@VERSION@@:$(VERSION):' META.in > META

SOURCES = $(wildcard *.mli) $(wildcard *.ml)

MLI = $(filter %.mli, $(SOURCES))
ML = $(filter %.ml, $(SOURCES))
CMI = $(ML:.ml=.cmi)
CMO = $(ML:.ml=.cmo)
CMX = $(ML:.ml=.cmx)
O = $(ML:.ml=.o)

aiger.cma: $(SOURCES) Makefile
	ocamlfind ocamlc -a $(FLAGS) -o aiger.cma \
		 $(SOURCES)

aiger.cmxa: $(SOURCES) Makefile
	ocamlfind ocamlopt -a $(FLAGS) \
		-o aiger.cmxa $(SOURCES)

doc: doc/index.html
doc/index.html: $(MLI)
	mkdir -p doc
	ocamlfind ocamldoc -d doc -html $(MLI)

install: META
	ocamlfind install aiger META \
          $$(ls $(MLI) $(CMI) $(CMO) $(CMX) $(O) \
             aiger.cma aiger.cmxa aiger.a)

uninstall:
	ocamlfind remove aiger

.PHONY: clean

clean:
	rm -f *.o *.a *.cm[ioxa] *.cmxa *~ *.annot
	rm -f META
	rm -rf doc
