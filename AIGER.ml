type id = int

type vec = id list (* lsb first (head of list), msb last *)

exception Unassigned_register of string

type expr =
  | False
  | Input of string
  | Reg of string * bool
  | And of id * id
  | Not of id

module SMap = Map.Make(struct
  type t = string
  let compare = compare
end)

type t = {
  counter: int ref;
  expr_to_id: (expr, id) Hashtbl.t;
  id_to_expr: (id, expr) Hashtbl.t;
  inputs_name: (string, id) Hashtbl.t;
  inputs_id: (id, string) Hashtbl.t;
  regs_name: (string, id * id option * bool) Hashtbl.t;
  regs_id: (id, string * id option * bool) Hashtbl.t;
  outputs_name: id SMap.t ref;
  outputs_id: (id, string) Hashtbl.t;
}

let list_iteri f l =
  let count = ref 0 in
  List.iter (fun el ->
    f !count el; incr count) l

let unhash t id =
  Hashtbl.find t.id_to_expr id

let do_hash t e =
  try
    Hashtbl.find t.expr_to_id e
  with Not_found ->
    let fresh = !(t.counter) in
    incr t.counter;
    begin match e with
    | Input s ->
      Hashtbl.replace t.inputs_name s fresh;
      Hashtbl.replace t.inputs_id fresh s
    | Reg (s,b) ->
      Hashtbl.replace t.regs_name s (fresh, None, b);
      Hashtbl.replace t.regs_id fresh (s, None, b)
    | _ -> ()
    end;
    Hashtbl.replace t.expr_to_id e fresh;
    Hashtbl.replace t.id_to_expr fresh e;
    fresh

let hash t e =
  let falseid = do_hash t False in
  let trueid = do_hash t (Not falseid) in
  match e with
  | Not id when id = trueid ->
    falseid
  | Not id when id = falseid ->
    trueid
  | Not id ->
    begin match unhash t id with
    | Not id -> id
    | _ -> do_hash t e
    end
  | And (id1, id2) ->
    begin match (unhash t id1, id1, unhash t id2, id2) with
    | (Not a, _, _, b) when a = b ->
      do_hash t False
    | (_, a, Not b, _) when a = b ->
      do_hash t False
    | (False, _, _, _) ->
      do_hash t False
    | (_, _, False, _) ->
      do_hash t False
    | (_, a, _, b) when a = b ->
      a
    | (_, a, _, b) when b = trueid ->
      a
    | (_, a, _, b) when a = trueid ->
      b
    | _ ->
      do_hash t (And (id1, id2))
    end
  | _ ->
    do_hash t e
      

let mk_aiger () =
  let t = {
    counter = ref 0;
    expr_to_id = Hashtbl.create 1023;
    id_to_expr = Hashtbl.create 1023;
    inputs_name = Hashtbl.create 29;
    inputs_id = Hashtbl.create 29;
    regs_name = Hashtbl.create 29;
    regs_id = Hashtbl.create 29;
    outputs_name = ref (SMap.empty);
    outputs_id = Hashtbl.create 29;
  } in
  ignore(do_hash t False);
  t


(* basic routines *)

let mk_false t =
  hash t False
    
let mk_and t id1 id2 =
  hash t (And (id1, id2))

let mk_not t id =
  hash t (Not id)

let mk_input t s =
  hash t (Input s)

let mk_reg t s =
  hash t (Reg (s, false))

let mk_reg_init t s i =
  hash t (Reg (s, i))

let set_reg_name t s inp_id =
  let (id, _inp, b) = Hashtbl.find t.regs_name s in
  Hashtbl.replace t.regs_name s (id, Some inp_id, b);
  Hashtbl.replace t.regs_id id (s, Some inp_id, b)

let set_reg_id t id inp_id =
  let (s, _inp, b) = Hashtbl.find t.regs_id id in
  Hashtbl.replace t.regs_name s (id, Some inp_id, b);
  Hashtbl.replace t.regs_id id (s, Some inp_id, b);
  Printf.printf "Assigning latch %d.next = %d\n" id inp_id

let mk_output t name id =
  t.outputs_name := SMap.add name id !(t.outputs_name);
  Hashtbl.replace t.outputs_id id name

(* derived routines *)

let mk_true t =
  mk_not t (mk_false t)

let mk_or t id1 id2 =
  mk_not t (mk_and t (mk_not t id1) (mk_not t id2))

let mk_nand t id1 id2 =
  mk_not t (mk_and t id1 id2)

let mk_nor t id1 id2 =
  mk_not t (mk_or t id1 id2)

let mk_xor t id1 id2 =
  let c = mk_nand t id1 id2 in
  let a = mk_nand t id1 c in
  let b = mk_nand t id2 c in
  mk_nand t a b

let mk_xnor t id1 id2 =
  mk_not t (mk_xor t id1 id2)

let mk_implies t id1 id2 =
  mk_or t (mk_not t id1) id2

let mk_eq = mk_xnor

let mk_ne = mk_xor

let mk_ite t id_c id_t id_e =
  mk_and t
    (mk_implies t id_c id_t)
    (mk_implies t (mk_not t id_c) id_e)

let mk_half_add t id1 id2 =
  let c = mk_and t id1 id2 in
  let s = mk_xor t id1 id2 in
  (c,s)

let mk_full_add t id1 id2 id3 =
  let s0 = mk_xor t id1 id2 in
  let s = mk_xor t s0 id3 in
  let a0 = mk_and t s0 id3 in
  let a1 = mk_and t id1 id2 in
  let c = mk_or t a0 a1 in
  (c, s)


(* vector operations *)

let vec_and t v1 v2 =
  List.map2 (mk_and t) v1 v2

let vec_or t v1 v2 =
  List.map2 (mk_or t) v1 v2

let vec_xor t v1 v2 =
  List.map2 (mk_xor t) v1 v2

let vec_not t v =
  List.map (mk_not t) v

let vec_int t i l =
  let res = ref [] in
  for bit = (l - 1) downto 0 do
    res := (if ((i asr bit) land 1) = 1 then
        mk_true t
      else
        mk_false t) :: !res
  done;
  let after = i asr l in
  if after <> (-1) && after <> 0 then
    raise (Invalid_argument "Not enough bits to contain constant");
  !res

let vec_big_int t i l =
  let res = ref [] in
  for bit = (l - 1) downto 0 do
    (* get a big in with the target bit set *)
    let pwr = Big_int.shift_left_big_int Big_int.unit_big_int bit in
    let set = Big_int.and_big_int i pwr in
    res := (if (Big_int.sign_big_int set) <> 0 then
        mk_true t
      else
        mk_false t) :: !res
  done;
  !res
  

let vec_input t n i =
  let v = ref [] in
  for ind = 0 to i - 1 do
    v := (mk_input t (n ^ "_" ^ (string_of_int ind))) :: !v
  done;
  List.rev !v

let vec_output t n v =
  list_iteri (fun i el ->
    mk_output t (n ^ "_" ^ (string_of_int i)) el)
    v

let vec_reg t n l =
  let v = ref [] in
  for i = 0 to l - 1 do
    v := (mk_reg t (n ^ "_" ^ (string_of_int i))) :: !v
  done;
  List.rev !v

let vec_reg_init t n init l =
  let init_vec = ref [] in
  for bit = (l - 1) downto 0 do
    init_vec := (((init asr bit) land 1) = 1) :: !init_vec
  done;
  let v = ref [] in
  list_iteri (fun i init ->
    v := (mk_reg_init t (n ^ "_" ^ (string_of_int i)) init) :: !v
  ) !init_vec;
  List.rev !v
  

let vec_set_reg_name t n e =
  try
    list_iteri (fun i el ->
      set_reg_name t (n^"_"^(string_of_int i)) el
    ) e
  with Not_found ->
    raise (Invalid_argument "Register undefined")

let vec_set_reg_id t ids e =
  List.iter2 (fun id el ->
    set_reg_id t id el
  ) ids e


let vec_inc t v =
  match v with
  | [] -> []
  | b0::rest ->
    let r0 = mk_not t b0 in
    let c = ref b0 in
    let rn = List.fold_left (fun v bn ->
      let (cn, rn) = mk_half_add t !c bn in
      c := cn;
      rn::v) [] rest in
    r0::(List.rev rn)

let vec_neg t v =
  vec_inc t (vec_not t v)

let vec_addc t v1 v2 cin =
  let c = ref cin in
  let s = List.map2 (fun b1 b2 ->
    let (cout,sn) = mk_full_add t b1 b2 !c in
    c := cout;
    sn
  ) v1 v2 in
  (!c, s)

let vec_add t v1 v2 =
  let (_c,s) = vec_addc t v1 v2 (mk_false t) in
  s

let vec_sub t v1 v2 =
  let v2' = vec_not t v2 in
  let (_c,s) = vec_addc t v1 v2' (mk_true t) in
  s

let vec_extend_val t i extval v =
  assert(i >= 0);
  let ext = ref [] in
  for c = 0 to i - 1 do
    ext := extval :: !ext
  done;
  v @ !ext

let vec_zero_extend t i v =
  vec_extend_val t i (mk_false t) v

let vec_one_extend t i v =
  vec_extend_val t i (mk_true t) v

let vec_sign_extend t i v =
  let extval = ref (mk_false t) in
  List.iter (fun el -> extval := el)  v;
  vec_extend_val t i (!extval) v

let rec list_drop i = function
  | [] -> []
  | _ when i < 0 ->
    raise (Invalid_argument "Argument out of range")
  | _ as l when i = 0 ->
    l
  | h::t ->
    list_drop (i-1) t

let rec list_take i = function
  | _ when i = 0 ->
    []

  | [] ->
    raise (Invalid_argument "Argument out of range")
  | _ when i < 0 ->
    raise (Invalid_argument "Argument out of range")

  | h::t ->
    let rest = list_take (i-1) t in
    h::rest

let vec_extract t b e v =
  let low = min b e in
  let high = max b e in
  let v' = list_drop low v in
  list_take (high-low+1) v'
(*
  let rec extract prog v =
    match v with
    | h::t when low < prog && prog = high ->
      [h]
    | h::t when low <= prog && prog < high ->
      h::(extract (prog+1) t)
    | h::t when low > prog ->
      extract (prog+1) t
    | _ ->
      raise (Invalid_argument "Argument out of range")
  in
  extract 0 v
*)

let vec_len _t v =
  List.length v

let vec_concat _t a b =
  b @ a

let vec_construct _t l = l

  

let vec_red_and t v =
  List.fold_left (fun red el ->
    mk_and t red el) (mk_true t) v

let vec_red_or t v =
  List.fold_left (fun red el ->
    mk_or t red el) (mk_false t) v

let vec_red_xor t v =
  List.fold_left (fun red el ->
    mk_xor t red el) (mk_false t) v

let vec_eq t v1 v2 =
  vec_red_and t (List.rev_map2 (mk_eq t) v1 v2)

let vec_ne t v1 v2 =
  mk_not t (vec_eq t v1 v2)

let vec_is_neg t v =
  let res = ref (mk_false t) in
  List.iter (fun el ->
    res := el
  ) v;
  !res

let vec_slt t v1 v2 =
  vec_is_neg t (vec_sub t v1 v2)

let vec_sgt t v1 v2 =
  vec_slt t v2 v1

let vec_sle t v1 v2 =
  mk_not t (vec_sgt t v1 v2)

let vec_sge t v1 v2 =
  mk_not t (vec_slt t v1 v2)


(*
  h1 h2 | out
  0  0  | acc
  0  1  | 1
  1  0  | 0
  1  1  | acc
*)
let rec vec_ult t acc v1 v2 =
  match (v1,v2) with
  | ([],[]) -> acc
  | (h1::t1, h2::t2) ->
    let acc' = mk_ite t (mk_eq t h1 h2) acc h2 in
    vec_ult t acc' t1 t2
  | _ -> raise (Invalid_argument "Vectors must be the same length")

let vec_ult t v1 v2 =
  vec_ult t (mk_false t) v1 v2

let vec_ugt t v1 v2 =
  vec_ult t v2 v1

let vec_uge t v1 v2 =
  mk_not t (vec_ult t v1 v2)

let vec_ule t v1 v2 =
  mk_not t (vec_ult t v2 v1)

let vec_ite t c te ee =
  List.map2 (fun te ee ->
    mk_ite t c te ee
  ) te ee


let rec list_take i l =
  if i <= 0 then
    []
  else
    match l with
    | [] -> []
    | h::tl ->
      h::(list_take (i-1) tl)

let rec list_drop i l =
  if i <= 0 then
    l
  else
    match l with
    | [] -> []
    | h::tl ->
      list_drop (i-1) tl

let rec list_put_rep i v l =
  if i <= 0 then
    l 
  else
    v::(list_put_rep (i-1) v l)
  

let vec_lsl t v i =
  assert(v <> []);
  assert(i >= 0);
  let l = List.length v in
  let v = list_take (l - i) v in
  list_put_rep i (mk_false t) v

let vec_lsr t v i =
  assert(v <> []);
  assert(i >= 0);
  let v = list_drop i v in
  vec_zero_extend t i v

let vec_asr t v i =
  assert(v <> []);
  assert(i >= 0);
  let v = list_drop i v in
  vec_sign_extend t i v

  

let vec_mul t v1 v2 =
  let l = List.length v1 in
  if l <> (List.length v2) then
    raise (Invalid_argument "Multiplicands have different lengths");
  let sum = ref (vec_int t 0 l) in
  list_iteri (fun i el ->
    sum := vec_add t !sum (vec_ite t el (vec_lsl t v1 i) (vec_int t 0 l))
  ) v2;
  !sum
  
  
  


(* output routines *)

module IMap = Map.Make(struct
  type t = int
  let compare = compare
end)

let write_aag t out =
  (* routine for fresh indices *)
  let max_index = ref 2 in
  let fresh_index () =
    let res = !max_index in
    incr max_index;
    incr max_index;
    res
  in

  (* maps for output *)
  let id_to_index = ref IMap.empty in
  let index_to_id = Hashtbl.create 1023 in
  let input_names = ref [] in
  let output_names = ref [] in
  let latch_names = ref [] in

  let add_id_index id index =
    id_to_index := IMap.add id index !id_to_index;
    Hashtbl.replace index_to_id index id
  in

  let add_input_name n id =
    input_names := (n,id) :: !input_names
  in
  let add_output_name n id =
    output_names := (n,id) :: !output_names
  in
  let add_latch_name n i id index input =
    Printf.printf "Latch: %s %B %d %d %d\n" n i id index input;
    latch_names := (n,i,id,index,input) :: !latch_names
  in

  (* add false and true to start *)
  add_id_index (mk_false t) 0;
  add_id_index (mk_true t) 1;

  (* process inputs *)
  Hashtbl.iter (fun name id ->
    let index = fresh_index () in
    add_id_index id index;
    add_input_name name id
  ) t.inputs_name;

  (* process latches/regs *)
  Hashtbl.iter (fun name (id, input, b) ->
    let index = fresh_index () in
    add_id_index id (index+(if b then 1 else 0));
    let input = match input with
      | Some input -> input
      | None -> raise (Unassigned_register name) in
    add_latch_name name b id index input
  ) t.regs_name;

  (* process and gates *)
  let and_count = ref 0 in
  Hashtbl.iter (fun expr id ->
    match expr with
    | Not _ -> ()
    | Input _ -> ()
    | Reg _ -> ()
    | False -> ()
    | And _ ->
      let index = fresh_index () in
      add_id_index id index;
      incr and_count
  ) t.expr_to_id;

  (* process not gates *)
  Hashtbl.iter (fun expr id ->
    match expr with
    | Not pid ->
      let index = IMap.find pid !id_to_index in
      add_id_index id (index+1)
    | Input _ -> ()
    | Reg _ -> ()
    | False -> ()
    | And _ -> ()
  ) t.expr_to_id;


  (* begin output process *)

  (* compute counts *)
  let ninputs = Hashtbl.length t.inputs_name in
  let nlatches = Hashtbl.length t.regs_name in
  let noutputs = SMap.cardinal !(t.outputs_name) in
  let nands = !and_count in
  let nvars = ninputs + nlatches + nands in

  (* print header line *)
  Printf.fprintf out "aag %d %d %d %d %d\n" nvars ninputs nlatches noutputs nands;


  (* print inputs *)
  let input_names = List.rev !input_names in
  List.iter (fun (name,id) ->
    Printf.fprintf out "%d\n" (IMap.find id !id_to_index)
  ) input_names;
  
  (* print latches *)
  let latch_names = List.rev !latch_names in
  List.iter (fun (name, init, id, index, input) ->
    let input = IMap.find input !id_to_index in
    let input = if init then
        input lxor 1
      else
        input
    in
    Printf.fprintf out "%d %d\n" index input
  ) latch_names;





    
    (*let (name, inp_id_opt, b) = Hashtbl.find t.regs_id id in
    assert(b = init);
    let index = IMap.find id !id_to_index in
    let index = index land (-2) in
    let inp_index = match inp_id_opt with
      | Some inp_id ->
        let inp_index = IMap.find inp_id !id_to_index in
        if init then
          inp_index lxor 1
        else
          inp_index
      | None -> assert false
    in
    Printf.fprintf out "%d %d\n" index inp_index
  ) latch_names;*)

  (* print outputs *)
  SMap.iter (fun name id ->
    let index = IMap.find id !id_to_index in
    add_output_name name id;
    Printf.fprintf out "%d\n" index
  ) !(t.outputs_name);

  (* print ands *)
  IMap.iter (fun id index ->
    match unhash t id with
    | False -> ()
    | Input _ -> ()
    | Reg _ -> ()
    | Not _ -> ()
    | And (a, b) ->
      let a_ind = IMap.find a !id_to_index in
      let b_ind = IMap.find b !id_to_index in
      Printf.fprintf out "%d %d %d\n" index a_ind b_ind
  ) !id_to_index;

  (* print input names *)
  list_iteri (fun i (name,_id) ->
    Printf.fprintf out "i%d %s\n" i name
  ) input_names;

  (* print output names *)
  list_iteri (fun i (name,_id) ->
    Printf.fprintf out "o%d %s\n" i name
  ) (List.rev !output_names);

  (* print latch names *)
  Printf.fprintf out "c\n";
  list_iteri (fun i (name,_init,_id,_index,_input) ->
    Printf.fprintf out "l%d %s\n" i name
  ) latch_names



let n_to_aig_bin n =
  let n = ref n in
  let res = ref [] in
  while !n <> 0 do
    let dig = (!n land 127) + 128 in
    n := !n lsr 7;
    res := dig :: !res
  done;
  match !res with
  | [] -> [ 0 ]
  | h::t ->
    List.rev ((h land 127)::t)

let write_aig t out =
  (* routine for fresh indices *)
  let max_index = ref 2 in
  let fresh_index () =
    let res = !max_index in
    incr max_index;
    incr max_index;
    res
  in

  (* count the number of and gates in the aig *)
  let and_count = ref 0 in
  Hashtbl.iter (fun expr id ->
    match expr with
    | And _ ->
      incr and_count
    | _ -> ()
  ) t.expr_to_id;
  let ninputs = Hashtbl.length t.inputs_name in
  let nlatches = Hashtbl.length t.regs_name in
  let noutputs = SMap.cardinal !(t.outputs_name) in
  let nands = !and_count in
  let nvars = ninputs + nlatches + nands in
  Printf.fprintf out "aig %d %d %d %d %d\n" nvars ninputs nlatches noutputs nands;

  (* maps for output *)
  let id_to_index = ref IMap.empty in
  let add_id_index id index =
    id_to_index := IMap.add id index !id_to_index
  in

  
  let to_process = ref [] in

  (* process inputs *)
  let i = ref 0 in
  let input_names = Buffer.create 10000 in
  Hashtbl.iter (fun name id ->
    let index = fresh_index () in
    Printf.bprintf input_names "i%d %s\n" !i name;
    incr i;
    add_id_index id index;
  ) t.inputs_name;

  (* process latches/regs *)
  let i = ref 0 in
  let latch_names = Buffer.create 10000 in
  Hashtbl.iter (fun name (id, input, b) ->
    let index = fresh_index () in
    let index = index+(if b then 1 else 0) in
    add_id_index id index;
    let input = match input with
      | Some input -> input
      | None -> raise (Unassigned_register name) in
    Printf.bprintf latch_names "l%d %s\n" !i name;
    incr i;
    Printf.fprintf out "%d\n" input;
    to_process := input :: !to_process
  ) t.regs_name;

  (* add outputs to the to_process list *)
  SMap.iter (fun name id ->
    to_process := id :: !to_process
  ) !(t.outputs_name);


  (* process and gates *)

  let ands = ref [] in
  let rec process_id id =
    try
      IMap.find id !id_to_index
    with Not_found ->
      match Hashtbl.find t.id_to_expr id with
      | False ->
        add_id_index id 0;
        0
      | Input _ ->
        assert false
      | Reg _ ->
        assert false
      | And (a, b) ->
        let aindex = process_id a in
        let bindex = process_id b in
        let oindex = fresh_index () in
        add_id_index id oindex;
        ands := (oindex, aindex, bindex) :: !ands;
        oindex
      | Not a ->
        let index = process_id a in
        let index = index lxor 1 in
        add_id_index id index;
        index
  in

  List.iter (fun id -> ignore(process_id id)) !to_process;

  (* print outputs *)
  let output_names = Buffer.create 10000 in
  let i = ref 0 in
  SMap.iter (fun name id ->
    let index = IMap.find id !id_to_index in
    Printf.fprintf out "%d\n" index;
    Printf.bprintf output_names "o%d %s\n" !i name;
    incr i
  ) !(t.outputs_name);

  (* print ands *)
  List.iter (fun (o, a, b) ->
    (* don't output out *)

    let (a,b) = if a > b then (a,b) else (b,a) in
    let delta0 = o - a in
    let delta1 = a - b in
    assert(delta0 > 0);
    assert(delta1 >= 0);
    List.iter (output_byte out) (n_to_aig_bin delta0);
    List.iter (output_byte out) (n_to_aig_bin delta1)
  ) (List.rev !ands);

  (* print input names *)
  Buffer.output_buffer out input_names;

  (* print output names *)
  Buffer.output_buffer out output_names;

  (* print latch names as comment *)
  Printf.fprintf out "c\n";

  Buffer.output_buffer out latch_names
