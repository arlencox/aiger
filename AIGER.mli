(** AIGER outputs sequential and combinational And-Inverter graphs in the AIGER
    1 text-mode format.  It uses the AAG type header and can be converted into
    the binary AIGER format using the [aigtoaig] tool provided in the
    {{: http://fmv.jku.at/aiger/}AIGER package}.

    It hash-conses the generated formulas to reduce the representation and to
    ensure good use of the AIG file format.  Additionally, it performs
    on-the-fly simplification and constant propagation.  It does not support
    advanced simplification techniques (such as sweeping), but all constants are
    correctly propagated.

    The generated file is compatible with model checkers that support the AAG
    input type or support the binary AIGER format that can be generated using
    the [aigtoaig] tool.

    An And-Inverter graph (AIG) is made up of the following elements:

    {ul {- Inputs are named, single-bit inputs from external sources.}
        {- Outputs are named, single-bit outputs to external sources.  They are
           used for properties in model checking.}
        {- And gates are two-input operations that perform the logical {b AND}
           of the two inputs.  That is, the output is true only if both inputs
           are true. }
        {- Inverter gates are single input operations that negate the input.}
        {- Registers are named sequential elements that pass their input through
           to their output whenever a clock edge occurs.  Clocks are implicit in
           AIGER representation. }
    }

    All other operations are constructed using these basic operations.

    Since AIGER does not support named registers and having register names is
    useful for debugging (especially when simplification has been applied),
    register names are included as comments at the end of the file.
*)

(** [t] represents the And-Inverter graph that is currently under construction *)
type t


(** [id] represents a node in the Ant-Inverter graph.  This always represents a
    single bit. *)
type id

(** [vec] represents a vector of bits.  Bit-vector operations operate on this
    data-type *)
type vec

(** If a register is unassigned when writing an AAG file this will return the register name. *)
exception Unassigned_register of string

val mk_aiger : unit -> t
(** Make a new AIGER context.  This corresponds to a single expression graph. *)




(** {2 Single Bit Operations} *)


(**
   These operations all take single bits of type [id] as arguments and return arguments with type [id].
   Operations are standard combinational operations that are standard in boolean logic.  All of these
   routines start with [mk_] and all take a context [t] as the first argument.
*)
    


  

(** {3 Inputs, Outputs and Registers} *)


val mk_input : t -> string -> id
(** [AIGER.mk_input t n] makes a named input with name [n].  A context, [t], must be provided. *)

val mk_output : t -> string -> id -> unit
(** [AIGER.mk_output t n e] makes a named output with name [n] that is driven by expression [e].
    A context, [t], must be provided. *)

val mk_reg : t -> string -> id
(** [AIGER.mk_reg t n] makes a register with name [n] and an undefined input that is reset to
    false.  Result is the [id] representing the output of the register.  A context, [t], must be
    provided. *)

val mk_reg_init : t -> string -> bool -> id
(** [AIGER.mk_reg_init t n i] makes a register with name [n] and an undefined input that is reset to
    [i].  Result is the [id] representing the outptu of the register.  A context, [t], must be
    provided. *)

(* set a registers input by name *)
val set_reg_name : t -> string -> id -> unit
(** [AIGER.set_reg_name t n e] assigns the input of the register named [n] to [e].
    A context, [t], must be provided. *)

val set_reg_id : t -> id -> id -> unit
(** [AIGER.set_reg_name t o e] assigns the input of the register with output [o] to [e].
    A context, [t], must be provided. *)






(** {3 Basic Boolean Operations} *)
  

(* make a false value *)
val mk_false : t -> id
(** [AIGER.mk_false t] returns an expression that is always {b false}.
    A context, [t], must be provided. *)

val mk_true : t -> id
(** [AIGER.mk_true t] returns an expression that is always {b true}.
    A context, [t], must be provided. *)

val mk_not : t -> id -> id
(** [AIGER.mk_not t e] returns an expression that negates the input [e].
    A context, [t], must be provided. *)

val mk_and : t -> id -> id -> id
(** [AIGER.mk_and t a b] returns an expression that is true when both [a] and [b] are true, otherwise false.
    A context, [t], must be provided. *)

val mk_or : t -> id -> id -> id
(** [AIGER.mk_or t a b] returns an expression that is true when either [a] or [b] is true, otherwise false.
    A context, [t], must be provided. *)

val mk_xor : t -> id -> id -> id
(** [AIGER.mk_xor t a b] returns an expression that is true when either [a] or [b], but not both are true, otherwise false.
    A context, [t], must be provided. *)

val mk_nand : t -> id -> id -> id
(** [AIGER.mk_nand t a b] returns an expression that is false when both [a] and [b] are true, otherwise true.
    A context, [t], must be provided. *)

val mk_nor : t -> id -> id -> id
(** [AIGER.mk_nor t a b] returns an expression that is false when either [a] or [b] is true, otherwise true.
    A context, [t], must be provided. *)

val mk_xnor : t -> id -> id -> id
(** [AIGER.mk_xor t a b] returns an expression that is false when either [a] or [b], but not both are true, otherwise true.
    A context, [t], must be provided. *)

val mk_implies : t -> id -> id -> id
(** [AIGER.mk_implies t p q] returns an expression that is true if when [p] is true, [q] is true, otherwise false.
    A context, [t], must be provided. *)

val mk_eq : t -> id -> id -> id
(** [AIGER.mk_eq t a b] returns an expression that is true when [a] is equal to [b], otherwise false.
    A context, [t], must be provided. *)

val mk_ne : t -> id -> id -> id
(** [AIGER.mk_ne t a b] returns an expression that is true when [a] is not equal to [b], otherwise false.
    A context, [t], must be provided. *)

val mk_ite : t -> id -> id -> id -> id
(** [AIGER.mk_ite t c a b] returns an expression that equal to [a] if [c] is true and equal to [b] if [c] is false.
    A context, [t], must be provided. *)






  
(** {3 Complex Boolean Operations} *)

  
val mk_half_add : t -> id -> id -> id * id
(** [AIGER.mk_half_add t a b] returns a two bit tuple [(c,s)], where
    [(c,s)] is the sum of the two bits [a] and [b].  The following truth table represents the operation:
    {v
    a  b  |  c  s
    ------+------
    0  0  |  0  0
    0  1  |  0  1
    1  0  |  0  1
    1  1  |  1  0
    v}
    A context, [t], must be provided.
*)

val mk_full_add : t -> id -> id -> id -> id * id
(** [AIGER.mk_full_add t a b i] returns a two bit tuple [(c,s)], where
    [(c,s)] is the sum of the three bits [a], [b] and [c].
    The following truth table represents the operation:
    {v
    a  b  i  |  c  s
    ---------+------
    0  0  0  |  0  0
    0  0  1  |  0  1
    0  1  0  |  0  1
    0  1  1  |  1  0
    1  0  0  |  0  1
    1  0  1  |  1  0
    1  1  0  |  1  0
    1  1  1  |  1  1
    v}
    A context, [t], must be provided.
*)








(** {2 Bit-Vector Operations} *)

(**
   Bit-Vector operations operate on vectors of bits instead of single bits.  These are all implemented
   in terms of the single bit operations.  These are simple implementations of the digital logic functions.
   All of these routines start with [vec_] and take a context, [t], as the first argument, like above.
*)


  
(** {3 Inputs, Outputs, Registers and Constants} *)


(* create a vector input (bits named _<num>) *) 
val vec_input : t -> string -> int -> vec
(** [AIGER.vec_input t n s] makes an input of size [s] with name [n].  The bits are
    named [n] followed by [_0], [_1], etc.  If this causes a name conflict with a
    bit level operation above the same input will be used.
    A context, [t], must be provided.
*)
  
val vec_output : t -> string -> vec -> unit
(** [AIGER.vec_input t n e] makes an output with name [n] that is assigned to the expression [e].
    The bits are named [n] followed by [_0], [_1], etc.  If this causes a name conflict with a bit
    level operation above the output will be reassigned.
    A context, [t], must be provided.
*)

val vec_reg : t -> string -> int -> vec
(** [AIGER.vec_reg t n l] makes a register with name [n] that is reset to 0.
    The bits are named [n] followed by [_0], [_1], etc.  If this causes a name conflict with a bit
    level operation above the same register will be used.
    A context, [t], must be provided.
*)

val vec_reg_init : t -> string -> int -> int -> vec
(** [AIGER.vec_reg_init t n i l] makes a register with name [n] that is reset to i.
    The bits are named [n] followed by [_0], [_1], etc up to [l]-1.  If this causes a name
    conflict with a bit level operation above the same register will be used.
    A context, [t], must be provided.
*)

val vec_set_reg_name : t -> string -> vec -> unit
(** [AIGER.vec_set_reg_name t n e] assigns the input to the registers described by name [n] to the
    provided vector [e].  A vector that is too short leaves upper register bits unassigned.  A vector
    that is too long raises [Invalid_argument].
    A context, [t], must be provided.
*)

  
val vec_set_reg_id : t -> vec -> vec -> unit
(** [AIGER.vec_set_reg_id t i e] assigns the input to the registers described by output [i] to the
    provided vector [e].
    A context, [t], must be provided.
*)

val vec_int : t -> int -> int -> vec
(** [AIGER.vec_int t i s] makes a constant expression with integer value [i], of length [s].
    A context, [t], must be provided.
*)

  
  

(** {3 Bitwise Operations } *)

val vec_and : t -> vec -> vec -> vec
(** [AIGER.vec_and t a b] makes a bitwise and operation between [a] and [b].  This returns a vector
    matching the length of both inputs where each bit represents the {b and} operation applied to
    each corresponding bit.  Raise [Invalid_argument] if the two vectors have different lengths.
    A context, [t], must be provided.
*)
    
val vec_or : t -> vec -> vec -> vec
(** [AIGER.vec_or t a b] makes a bitwise or operation between [a] and [b].  This returns a vector
    matching the length of both inputs where each bit represents the {b or} operation applied to
    each corresponding bit.  Raise [Invalid_argument] if the two vectors have different lengths.
    A context, [t], must be provided.
*)

val vec_xor : t -> vec -> vec -> vec
(** [AIGER.vec_xor t a b] makes a bitwise xor operation between [a] and [b].  This returns a vector
    matching the length of both inputs where each bit represents the {b xor} operation applied to
    each corresponding bit.  Raise [Invalid_argument] if the two vectors have different lengths.
    A context, [t], must be provided.
*)

val vec_not : t -> vec -> vec
(** [AIGER.vec_not t e] makes a bitwise not operation on [e].  This returns a vector of matching
    length where each bit has been flipped.  A context, [t], must be provided.
*)

val vec_lsl : t -> vec -> int -> vec
(** [AIGER.vec_lsl t e i] makes a logical shift left of [e] by [i] bits.  This truncates the upper
    [i] bits and inserts [i] falses/0s in the least significant positions.  Shift amount must be
    greater than or equal to zero.  A context, [t], must be provided.
*)

val vec_lsr : t -> vec -> int -> vec
(** [AIGER.vec_lsl t e i] makes a logical shift right of [e] by [i] bits.  This truncates the lower
    [i] bits and inserts [i] falses/0s in the most significant positions.  Shift amount must be
    greater than or equal to zero.  A context, [t], must be provided.
*)

val vec_asr : t -> vec -> int -> vec
(** [AIGER.vec_lsl t e i] makes a logical shift right of [e] by [i] bits.  This truncates the lower
    [i] bits and inserts [i] copies of the most significant bit in the most significant positions. 
    Shift amount must be greater than or equal to zero.  A context, [t], must be provided.
*)


(** {3 Primitive Arithmetic Operations } *)

val vec_inc : t -> vec -> vec
(** [AIGER.vec_inc t e] makes an increment operation on [e].  This returns a vector of matching
    length where, if an overflow occurs, the result wraps back around to 0.  A context, [t],
    must be provided.
*)

val vec_neg : t -> vec -> vec
(** [AIGER.vec_neg t e] makes a negation operation on [e].  This computes -[e] using 2's
    complement.  A context, [t], must be provided.
*)
  
val vec_addc : t -> vec -> vec -> id -> id * vec
(** [AIGER.vec_addc t a b i] makes an addition operation including a carry-in, [i].  It adds
    vector [a] to vector [b] including carry bit [i].  It produces the result [(c,s)], where
    [s] is the same length as both [a] and [b] and [c] is the carry out.  This is implemented
    using a ripple-carry adder and thus may be non-optimal for some operations.  Raise
    [Invalid_argument] if the two vectors have different lengths.  A context, [t], must be
    provided.
*)

(** {3 High-Level Arithmetic Operations} *)

val vec_add : t -> vec -> vec -> vec
(** [AIGER.vec_add t a b] makes an addition operation.  It adds vector [a] to vector [b].
    It produces the result [s], where [s] is the same length as both [a] and [b].  This
    is implemented using a ripple-carry adder and thus may be non-optimal for some operations.
    Overflow will wrap.  Raise [Invalid_argument] if the two vectors have different lengths.
    A context, [t], must be provided.
*)

val vec_sub : t -> vec -> vec -> vec
(** [AIGER.vec_sub t a b] makes a subtraction operation.  It subtracts vector [b] from vector
    [b]. It produces the result [s], where [s] is the same length as both [a] and [b].  This
    is implemented using a ripple-carry adder and thus may be non-optimal for some operations.
    Overflow will wrap.  Raise [Invalid_argument] if the two vectors have different lengths.
    A context, [t], must be provided.
*)

val vec_mul : t -> vec -> vec -> vec
(** [AIGER.vec_mul t a b] makes a multiplication operation.  It multiplies vector [a] with
    vector [b].  It produces a result [p] that is the same length as both [a] and [b].  Raise
    [Invalid_argument] if the two vectors have different lengths.  A context, [t], must be
    provided.
*)

(** {3 Comparison Operations} *)

val vec_slt : t -> vec -> vec -> id
(** [AIGER.vec_slt t a b] returns true if the signed number [a] is less than the signed
    number [b], otherwise false.  Raise [Invalid_argument] if the two vectors have different
    lengths.  A context, [t], must be provided.
*)
  
val vec_sgt : t -> vec -> vec -> id
(** [AIGER.vec_sgt t a b] returns true if the signed number [a] is greater than the signed
    number [b], otherwise false.  Raise [Invalid_argument] if the two vectors have different
    lengths.  A context, [t], must be provided.
*)

val vec_sle : t -> vec -> vec -> id
(** [AIGER.vec_sle t a b] returns true if the signed number [a] is less than or equal to
    the signed number [b], otherwise false.  Raise [Invalid_argument] if the two vectors
    have different lengths.  A context, [t], must be provided.
*)

val vec_sge : t -> vec -> vec -> id
(** [AIGER.vec_sge t a b] returns true if the signed number [a] is greater than or equal to
    the signed number [b], otherwise false.  Raise [Invalid_argument] if the two vectors
    have different lengths.  A context, [t], must be provided.
*)

val vec_ult : t -> vec -> vec -> id
(** [AIGER.vec_ult t a b] returns true if the unsigned number [a] is strictly less than the
    unsigned number [b], otherwise false.  Raise [Invalid_argument] if the two vectors
    have different lengths.  A context, [t], must must be provided.
*)

val vec_ugt : t -> vec -> vec -> id
(** [AIGER.vec_ugt t a b] returns true if the unsigned number [a] is strictly greater than the
    unsigned number [b], otherwise false.  Raise [Invalid_argument] if the two vectors
    have different lengths.  A context, [t], must must be provided.
*)

val vec_ule : t -> vec -> vec -> id
(** [AIGER.vec_ule t a b] returns true if the unsigned number [a] is less than or equal to the
    unsigned number [b], otherwise false.  Raise [Invalid_argument] if the two vectors
    have different lengths.  A context, [t], must must be provided.
*)

val vec_uge : t -> vec -> vec -> id
(** [AIGER.vec_uge t a b] returns true if the unsigned number [a] is greater than or equal to the
    unsigned number [b], otherwise false.  Raise [Invalid_argument] if the two vectors
    have different lengths.  A context, [t], must must be provided.
*)

val vec_eq : t -> vec -> vec -> id
(** [AIGER.vec_eq t a b] returns true if the vector [a] is equal to the vector [b], otherwise
    false.  Raise [Invalid_argument] if the two vectors have different lengths.  A context,
    [t], must be provided.
*)

val vec_ne : t -> vec -> vec -> id
(** [AIGER.vec_ne t a b] returns true if the vector [a] is not equal to the vector [b], otherwise
    false.  Raise [Invalid_argument] if the two vectors have different lengths.  A context,
    [t], must be provided.
*)


(** {3 Conversion and Extension Operations} *)
  
val vec_zero_extend : t -> int -> vec -> vec
(** [AIGER.vec_zero_extend t l e] extends the vector [e] with zeros, adding [l] bits.
    A context, [t], must be provided.
*)

val vec_one_extend : t -> int -> vec -> vec
(** [AIGER.vec_one_extend t l e] extends the vector [e] with ones, adding [l] bits.
    A context, [t], must be provided.
*)

val vec_sign_extend : t -> int -> vec -> vec
(** [AIGER.vec_one_extend t l e] extends the vector [e] with same value as the most
    significant bit of the provided vector, adding [l] bits.
    A context, [t], must be provided.
*)

val vec_extract : t -> int -> int -> vec -> vec
(** [AIGER.vec_extract t s e v] extracts bits [s] to [e] inclusive from vector [v]
    producing a vector that is abs([s]-[e])+1 bits long.
    Raise [Invalid_argument] if e or s is out of range.
    A context, [t], must be provided.
*)

val vec_len : t -> vec -> int
(** [AIGER.vec_len t v] returns the number of bits in the vector [v].  A context, [t],
    must be provided.
*)

val vec_concat : t -> vec -> vec -> vec
(** [AIGER.vec_concaat t a b] returns a vector that is constructed of the bits of [a]
    as the most significant and the bits of [b] as the least significant.
    A context, [t], must be provided *)

val vec_construct : t -> id list -> vec
(** [AIGER.vec_concaat t l] returns a vector that is constructed of the bits [l] where
    the first bit of [l] is the least significant bit and the last bit of [l] is the
    most significant bit.
    A context, [t], must be provided *)


(** {3 Reduction Operations} *)

val vec_red_and : t -> vec -> id
(** [AIGER.vec_red_and t e] gets a single bit from a vector by anding all of the bits
    together.  A vector of length zero returns true.
    A context, [t], must be provided.
*)
  
val vec_red_or : t -> vec -> id
(** [AIGER.vec_red_or t e] gets a single bit from a vector by oring all of the bits
    together.  A vector of length zero returns false.
    A context, [t], must be provided.
*)

val vec_red_xor : t -> vec -> id
(** [AIGER.vec_red_xor t e] gets a single bit from a vector by xoring all of the bits
    together.  A vector of length zero returns false.
    A context, [t], must be provided.
*)




(** {2 Output Routines} *)

val write_aag : t -> out_channel -> unit
(** [AIGER.write_aag t o] writes an AAG file representing the current context [t] to
    the output channel [o].
*)

val write_aig : t -> out_channel -> unit
(** [AIGER.write_aig t o] writes an AIG file representing the current context [t] to
    the output channel [o].
*)
