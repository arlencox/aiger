open AIGER
  
let _ =
  let t = mk_aiger () in
  let a = vec_int t (19) 5 in
  (*let b = vec_int t (-1) 5 in*)
  let c = vec_asr t a 1 in
  vec_output t "out" c;
  write_aig t (open_out "test.aig")
